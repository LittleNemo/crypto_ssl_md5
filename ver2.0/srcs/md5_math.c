/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ssl_md5.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 15:58:22 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/24 21:12:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include <stdlib.h>

/*
**	The md5_math_update() function updates the values used during the main loop
**	of the md5 algorythme.
*/
static void		md5_math_update(uint32_t *v, uint32_t *rot, uint32_t *sin, uint32_t *word)
{
	v[6] = v[3];
	v[3] = v[2];
	v[2] = v[1];
	v[1] += ft_rot_left((v[0] + v[4] + sin[v[7]] + word[v[5]]), rot[v[7]]);
	v[0] = v[6];
}

/*
**	The md5_math_loop() function is where the calculations of the hash of md5 is
**	done. This is the main loop of the algorythme. The updates of the values are
**	done in the md5_math_update() function.
*/
static void		md5_math_loop(uint32_t *val, uint32_t *rot, uint32_t *sin, uint32_t *word)
{
	val[7] = 0;
	while (val[7] < 64)
	{
		if (val[7] < 16)
			val[4] = (val[1] & val[2]) | ((~val[1]) & val[3]);
		else if (val[7] < 32)
			val[4] = (val[3] & val[1]) | ((~val[3]) & val[2]);
		else if (val[7] < 48)
			val[4] = val[1] ^ val[2] ^ val[3];
		else
			val[4] = val[2] ^ (val[1] | (~val[3]));
		if (val[7] < 16)
			val[5] = val[7];
		else if (val[7] < 32)
			val[5] = (5 * val[7] + 1) % 16;
		else if (val[7] < 48)
			val[5] = (3 * val[7] + 5) % 16;
		else
			val[5] = (7 * val[7]) % 16;
		md5_math_update(val, rot, sin, word);
		val[7]++;
	}
}

/*
**	The md5_math_init() function initializes the variables that will be used for
**	the calculations of the hash during the md5_math_loop() function.
*/
void			md5_math_init(uint32_t *h, char *new_str, size_t new_size)
{
	uint32_t		*rot;
	uint32_t		*sin;
	uint32_t		*word;
	uint32_t		val[8];
	size_t			offset;

	rot = md5_sgl_rot();
	sin = md5_sgl_sin();
	offset = 0;
	while (offset < new_size)
	{
		val[0] = h[0];
		val[1] = h[1];
		val[2] = h[2];
		val[3] = h[3];
		word = (uint32_t *)(new_str + offset);
		md5_math_loop(val, rot, sin, word);
		h[0] += val[0];
		h[1] += val[1]; 
		h[2] += val[2];
		h[3] += val[3];
		offset += (512 / 8);
	}
}