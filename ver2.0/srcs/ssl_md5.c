/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ssl_md5.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 15:58:22 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/24 21:12:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include <stdlib.h>
/*
**	The md5_cat_res() function transforms the array "h" from longs to strings
**	and concatenates them to produce a new string.
*/
static char		*md5_cat_res(uint32_t *h)
{
	char			*hash;
	char			**tmp;

	if (!(hash = ft_strnew(32)))
		return (NULL);
	if (!(tmp = (char **)ft_memalloc(sizeof(tmp) * 5)))
	{
		ft_strdel(&hash);
		return (NULL);
	}
	if (!(tmp[0] = md5_ltoa(h[0])) || !(tmp[1] = md5_ltoa(h[1])) || \
		!(tmp[2] = md5_ltoa(h[2])) || !(tmp[3] = md5_ltoa(h[3])))
	{
		ft_strdel(&hash);
		ft_tabdel((void **)tmp, ft_tablen(tmp));
		return (NULL);
	}
	ft_strcat(hash, md5_cat_invert(tmp[0]));
	ft_strcat(hash, md5_cat_invert(tmp[1]));
	ft_strcat(hash, md5_cat_invert(tmp[2]));
	ft_strcat(hash, md5_cat_invert(tmp[3]));
	ft_tabdel((void **)tmp, ft_tablen(tmp));
	return (hash);
}
/*
static char		*md5_cat_res(uint32_t *h)
{
	char			*hash;
	char			**tmp;
	uint8_t			*p;
	size_t			i;

	i = 0;
	if (!(tmp = (char **)ft_memalloc(sizeof(tmp) * 5)))
		return (NULL);
	while (i < 4)
	{
		p = (uint8_t *)&h[i];
		ft_asprintf(&tmp[i++], "%.2x%.2x%.2x%.2x", p[0], p[1], p[2], p[3]);
	}
	ft_asprintf(&hash, "%s%s%s%s", tmp[0], tmp[1], tmp[2], tmp[3]);
	ft_tabdel((void **)tmp, 4);
	return (hash);
}
*/

/*
** DESCRIPTION
**	The ssl_md5() function generates a hash code from the given string "str" of
**	length "size" (expresed in bytes). In the first time, the ssl_md5() function
**	computes a new size that is 448 mod 512 bits long to correctly padd the
**	original string. Next, the function generates a new string with the padding
**	and the original size wrote at the end of it. Then, the math magic happens.
**	And finally, the different parts of the hash value are appended to
**	one-another.
**
** RETURN VALUES
**	- ssl_md5() returns a pointer to a string that represent the hash value of
**	the original message str. If an error occurs during the process, a NULL
**	pointer is returned.
*/

char			*ssl_md5(char *str, size_t size)
{
	char			*new_str;
	size_t			new_size;
	uint32_t		h[4] = {0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476};
	uint32_t		true_size;

	new_size = ((((size + 8) / 64) + 1) * 64) - 8;
	if (!(new_str = (char *)ft_memalloc(new_size + 64)))
		return (NULL);
	ft_memcpy(new_str, str, size);
	new_str[size] = 0b10000000;
	true_size = size * 8;
	ft_memcpy(new_str + new_size, &true_size, 4);
	md5_math_init(h, new_str, new_size);
	ft_strdel(&new_str);
	return (md5_cat_res(h));
}

#include <stdio.h>
int				main() {
	char			*str;
	char			*res;
	char			*tab[] = {
		STR_PICKLE, STR_LIVING,STR_SPOTIFY, 
		STR_AZERTY,STR_LOREM_IPSUM, STR_LOREM_2,
		NULL
	};
	char			*tab_name[] = {
		"STR_PICKLE", "STR_LIVING","STR_SPOTIFY", 
		"STR_AZERTY","STR_LOREM_IPSUM", "STR_LOREM_2",
		NULL
	};

	printf("TEST MD5 START ========================================\n");
	for (size_t i = 0; i < ft_tablen(tab); i++) {
		str = tab[i];
		res = ssl_md5(str, ft_strlen(str));
		printf("%s  %s\n", res, tab_name[i]);
		ft_strdel(&res);
	}
	printf("TEST MD5 END ==========================================\n");
	// while (1)	;
	return (0);
}