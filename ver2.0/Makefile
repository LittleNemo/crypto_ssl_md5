# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/16 10:26:59 by lbrangie          #+#    #+#              #
#    Updated: 2019/04/25 15:36:07 by lbrangie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

################################################################################
##                                   TARGET                                   ##
################################################################################
NAME_SSL			=		ft_ssl


################################################################################
##                                   SOURCES                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
SRCS_DIR			=		srcs/

# ================================= FILES ================================= #
FILES_SSL			=		ssl_md5.c			\
							md5_math.c			\
							md5_sgl_rot.c		\
							md5_sgl_sin.c		\
							md5_utils.c

# ================================ SOURCES ================================ #
SRCS_SSL			=		$(addprefix $(SRCS_DIR), $(FILES_SSL))


################################################################################
##                                   OBJECTS                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
OBJS_DIR			=		objs/

# ================================ OBJECTS ================================ #
OBJS_SSL			=		$(addprefix $(OBJS_DIR), $(FILES_SSL:.c=.o))


################################################################################
##                                  COMPILING                                 ##
################################################################################
# ============================== COMPILATOR =============================== #
CC					=		gcc
CFLAGS				+=		-Wall -Wextra -Werror -Ofast

# =============================== INCLUDES ================================ #
INC_DIR				=		incs/
INC_FILE			=		ft_ssl.h						\
							ft_ssl_defines.h				\
							ft_ssl_typedefs.h

INC_COMP			=		-I$(INC_DIR) -Ilibs/libft/incs -Ilibs/ft_printf/incs
INC_SSL				=		$(addprefix $(INC_DIR), $(INC_FILE))

# =============================== LIBRARIES =============================== #
LIB_DIR				=		libs/

LIBPTF_PATH			=		./$(LIB_DIR)ft_printf/
LIBPTF_SRC_NAME		=		$(shell make -C $(LIBPTF_PATH) print-SRCS)
LIBPTF_SRC			=		$(addprefix $(LIBPTF_PATH), $(LIBPTF_SRC_NAME))
LIBPTF_ARCV			=		$(LIBPTF_PATH)libftprintf.a

LIBFT_PATH			=		./$(LIB_DIR)libft/
LIBFT_SRC_NAME		=		$(shell make -C $(LIBFT_PATH) print-SRCS)
LIBFT_SRC			=		$(addprefix $(LIBFT_PATH), $(LIBFT_SRC_NAME))
LIBFT_ARCV			=		$(LIBFT_PATH)libft.a

AR					=		ar rc
RAN					=		ranlib

# ================================ SLAVES ================================= #
MAKESLV				=		make -C


################################################################################
##                                  DELETION                                  ##
################################################################################
RM					=		rm -f
RM_DIR				=		rm -rfd


################################################################################
##                                   DISPLAY                                  ##
################################################################################
# ================================ COLORS ================================= #
RED				=		\033[31m
GRN				=		\033[32m
BLU				=		\033[34m
WHT				=		\033[97m
CYN				=		\033[36m
MGT				=		\033[35m
YLW				=		\033[33m
BCK				=		\033[30m
LBLU			=		\033[94m
PRL				=		\033[38;5;55m
ORG				=		\033[38;5;202m
GRE				=		\033[90m
SLM				=		\033[38;5;203m

# ================================ FORMAT ================================= #
BOLD			=		\033[1m
DIM				=		\033[2m
UNDER			=		\033[4m
BLINK			=		\033[5m
HIDDEN			=		\033[8m
INVERT			=		\033[7m
ITALIC			=		\033[3m
EOF				=		\033[0m

# ============================== CHARACTERS =============================== #
CHECK			=		\\xE2\\x9C\\x94
CROSS			=		\\xE2\\x9C\\x98
HGLSS			=		\\xE2\\xA7\\x97
RAROW			=		\\xE2\\x9E\\x9C
COPYR			=		\\xC2\\xA9

# ================================ MACROS ================================= #
OK				=		$(GRN)$(CHECK)$(EOF)
KO				=		$(RED)$(CROSS)$(EOF)
WAIT			=		$(LBLU)$(HGLSS)$(EOF)
ARROW			=		$(LBLU)$(RAROW)$(EOF)
COFFE			=		\t  S\n\t$(UNDER)C[_]$(EOF)


################################################################################
##                                    RULES                                   ##
################################################################################

all:			$(NAME_SSL)		## Compiles the ft_ssl executable file

proper:			## Compiles the ft_ssl executable file, then, removes the objs/ directory 
	@$(MAKESLV) . all
	@$(MAKESLV) . clean




$(OBJS_DIR):
	@mkdir -p $@
	@printf "\t$(UNDER)$(BOLD)Created$(EOF) : $@/ $(OK)\n"




$(LIBFT_ARCV):			$(LIBFT_SRC)
	@printf "\t$(UNDER)$(BOLD)Creation$(EOF) $@ $(WAIT)\n\t\tPlease, wait..."
	@$(MAKESLV) $(LIBFT_PATH) all
	@printf "\033[K\r"
	@printf "\033[1A"
	@printf "\033[K\r"
	@printf "\t$(UNDER)$(BOLD)Archive created$(EOF) : $@ $(OK)\n"

$(LIBPTF_ARCV):			$(LIBPTF_SRC)
	@printf "\t$(UNDER)$(BOLD)Creation$(EOF) $@ $(WAIT)\n\t\tPlease, wait..."
	@$(MAKESLV) $(LIBPTF_PATH) all
	@printf "\033[K\r"
	@printf "\033[1A"
	@printf "\033[K\r"
	@printf "\t$(UNDER)$(BOLD)Archive created$(EOF) : $@ $(OK)\n"




$(OBJS_DIR)%.o:			$(SRCS_DIR)%.c $(INC_SSL) Makefile
	@printf "\t$(UNDER)$(BOLD)Compiled$(EOF) : $@ $(OK)"
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)
	@printf "\033[2K\r"

$(NAME_SSL):			$(OBJS_DIR) $(LIBFT_ARCV) $(LIBPTF_ARCV) $(OBJS_SSL)
	@$(CC) $(CFLAGS) -o $@ $(OBJS_SSL) $(LIBPTF_ARCV) $(LIBFT_ARCV) $(INC_COMP)
	@printf "\t$(UNDER)$(BOLD)Executable created$(EOF) : $@ $(OK)            \n"




clean:			## Removes the objs/ directory
	@$(RM_DIR) $(OBJS_DIR)
	@printf "\t$(UNDER)$(BOLD)Removed$(EOF) : $(OBJS_DIR) $(OK)\n"
	@$(MAKESLV) $(LIBFT_PATH) clean
	@$(MAKESLV) $(LIBPTF_PATH) clean

fclean:			clean		## Removes the objs/ diretory and the executable file
	@$(RM) $(NAME_SSL)
	@printf "\t$(UNDER)$(BOLD)Removed$(EOF) : $(NAME_SSL) $(OK)\n"
	@$(MAKESLV) $(LIBFT_PATH) fclean
	@$(MAKESLV) $(LIBPTF_PATH) fclean
	@$(RM_DIR) $(NAME_SSL).dSYM

re:			## Clears the directory and recompiles the executable file
	@$(MAKESLV) . fclean
	@$(MAKESLV) . all

norme:			$(INC_DIR) $(SRCS_DIR)$(SRCS_DIR_SSL)		## Checkes the norme of the files into the srcs/ and incs/ directories (NOT WORKING CURRENTLY)
	@norminette $^ | grep -B 1 "Warning\|Error" || true
	@$(MAKESLV) $(LIBFT_PATH) clean
	@$(MAKESLV) $(LIBPTF_PATH) clean

coffee:			## Serves a cup of coffee
	@echo "Here's a cup of coffee\n$(COFFE)"

help:			## Displays this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[0m%-10s\033[90m %s\n", $$1, $$2}'
	
.PHONY:			all proper clean fclean re norme coffee


# make -C libft                                                                                                   
# gcc -o test.o -c main.c -Ilibft/incs -Ift_printf/incs; gcc test.o ft_printf/libftprintf.a libft/libft.a; ./a.out