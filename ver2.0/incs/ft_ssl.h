/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 15:58:22 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/24 21:12:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H

# include "libft_part_math.h"
# include "libft_part_memory.h"
# include "libft_part_string.h"
# include "ft_ssl_defines.h"

char			*ssl_md5(char *str, size_t size);
char			*md5_cat_invert(char *str);
char			*md5_ltoa(uint32_t value);
void			md5_math_init(uint32_t *h, char *new_str, size_t new_size);
uint32_t		*md5_sgl_rot(void);
uint32_t		*md5_sgl_sin(void);

#endif