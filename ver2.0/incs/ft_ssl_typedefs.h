#ifndef FT_SSL_TYPEDEFS_H
# define FT_SSL_TYPEDEFS_H

typedef struct	s_meta {
	short			opts;
	short			nb_opts;
	int				out_fd;
	char			*out_name;
}				t_meta;

typedef struct	s_task_h {
	unsigned int	len;
	struct s_task	*head;
	struct s_task	*tail;
}				t_task_h;

typedef struct	s_task {
	char			*f_name;
	char			*content;
	char			*hash;
	int				fd;
	t_bool			is_str;
	long			msg_size;
	size_t			pad;
	struct s_task_h	*header;
	struct s_task	*next;
	struct s_task	*prev;
}				t_task;

typedef enum	e_error {
	SSL_NO_ERROR,
	SSL_ERROR_HELP,
	SSL_ERROR_MALLOC,
	SSL_ERROR_READ
}				t_error;

typedef enum	e_dgst_opts {
	DGST_OTPS_OUT,
	DGST_OPTS_PRINT,
	DGST_OPTS_QUIET,
	DGST_OPTS_REVERSE
}				t_dgst_opts;

#endif
