#include "ft_ssl.h"

/*
**	The ssl_print_help() function prints the content of the file defined by ===
**	SSL_RSC_HELP on the error output. The SSL_RSC_HELP file are expected to ===
**	content the general instructions to make the ft_ssl program work ==========
**	properly. The modifications of the file will lead to wrong informations ===
**	to been display. ==========================================================
*/
int				ssl_print_help(char **array)
{
	(void)array;
	ft_putfile_fd(SSL_RSC_HELP, stderr->_fileno);
	return (0);
}

/*
**	The ssl_get_errors() function is a singleton that gives an array of the ===
**	global error messages. This function is called by the ssl_error function. =
*/
static char		**ssl_get_errors(void)
{
	static char		*tab[] = {
		SSL_MSG_HELP,
		SSL_MSG_MALLOC,
		SSL_MSG_READ,
		NULL
	};

	return (tab);
}

/*
**	The ssl_error() function handles the general errors that can occur during =
**	the execution such as malloc failure, printing of the usage, etc... =======
**	The function takes an error code, print the associated message, delete ====
**	the structures of the programm and exit with "EXIT_FAILURE" ===============
*/
void			ssl_error(int code, t_meta *meta, t_task_h *task)
{
	char			**tab;

	if (code == SSL_NO_ERROR)
		return ;
	tab = ssl_get_errors();
	fprintf(stderr, "Error (ft_ssl): %s\n", tab[code - 1]);
	ssl_del_meta(meta);
	ssl_del_task(task);
	exit(EXIT_FAILURE);
}