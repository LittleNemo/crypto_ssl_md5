#include "ft_ssl.h"

void			ft_memprnt(void *b, size_t len)
{
	size_t			i;
	char			c;

	i = 0;
	while (i < len)
	{
		c = ((char *)b)[i++];
		if (c < 32 || 126 < c)
			c = 46;
		write(1, &c, 1);
	}
}
void			*ft_memjoin(void *p1, void *p2, size_t l1, size_t l2)
{
	void			*fresh;

	if (!p1 || !p2 || !(fresh = ft_memalloc(l1 + l2)))
		return (NULL);
	ft_memcpy(fresh, p1, l1);
	ft_memcpy(fresh + l1, p2, l2);
	return (fresh);

}

static void		dgst_pad_content(t_meta *meta, t_task_h *task_h, t_task *tmp)
{
	char			*str;
	char			*todel;

	// Adding the 1 bit at the end of the message.
	if (!(str = (char *)ft_memalloc(1)) || !(ft_memset(str, '1', 1)))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	todel = tmp->content;
	if (!(tmp->content = ft_memjoin(todel, str, tmp->msg_size / 8, 1)) && \
		ft_strdel(&todel) && ft_strdel(&str))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);

	// Calculation of the number of 0 bits to add after the 1 bit and adding 
	// them.
	dgst_get_padding(tmp);
	if (!(str = ft_strnew(tmp->pad / 8)) || !ft_memset(str, DGST_NUL_BIT, tmp->pad / 8))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	todel = tmp->content;
	if (!(tmp->content = ft_strjoin(todel, str)) && \
		ft_strdel(&todel) && ft_strdel(&str))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
/*
	dgst_get_padding(tmp);
	if (!(str = (char *)ft_memalloc(tmp->pad / 8)) || !ft_memset(str, '0', tmp->pad / 8))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	todel = tmp->content;
	if (!(tmp->content = ft_memjoin(todel, str, (tmp->msg_size + 1) / 8, tmp->pad / 8)) && \
		ft_strdel(&todel) && ft_strdel(&str))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);

	// Adding the length of the mesage after the 0 bits.
	if (!(str = ft_strdup("SSSSSSSS")))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	todel = tmp->content;
	if (!(tmp->content = ft_strjoin(todel, str)) && \
		ft_strdel(&todel) && ft_strdel(&str))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
*/
	// ft_memset(&tmp->content[(tmp->msg_size / 8) + 1], '0', tmp->pad / 8);
}

/*
**	The dgst_read_file() function reads the content of the file of a given ====
**	task of the task list. It loops through the file until the end and stores =
**	the bytes read into the "content" field. If an error occurs, an error =====
**	message is copied into the "hash" field. ==================================
*/
static void		dgst_read_file(t_meta *meta, t_task_h *task_h, t_task *tmp)
{
	char			buff[BUFF_SIZE];
	char			*str;
	int				ret;

	ft_bzero(buff, BUFF_SIZE);
	if (!(tmp->content = ft_strdup(buff)))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	while ((ret = read(tmp->fd, buff, BUFF_SIZE)) > 0)
	{
		str = tmp->content;
		if (!(tmp->content = (char *)ft_memjoin(str, buff, tmp->msg_size, ret)))
			ssl_error(SSL_ERROR_MALLOC, meta, task_h);
		ft_bzero(buff, BUFF_SIZE);
		ft_strdel(&str);
		tmp->msg_size += ret;
	}
	if (ret != -1 && !tmp->content && !(tmp->content = ft_strdup("")))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	if (ret == -1)
		asprintf(&tmp->hash, "%s: %s", tmp->f_name, strerror(errno));
}

/*
**	The dgst_get_content() function goes through every links of task list and =
**	tries to open it. Two cases can occurs: first, the "is_str" field is ======
**	false, then, the file name is concidered to be the content of the file; ===
**	second, "is_str" is false but the file cannot be open, in that case, an ===
**	error message is copied to the "hash" field. After openning the file, the =
==	dgst_read_file() function is called to read the content of the file. ======
*/
static void		dgst_get_content(t_meta *meta, t_task_h *task_h)
{
	t_task			*tmp;

	tmp = task_h->head;
	while (tmp)
	{
		if (tmp->is_str && !(tmp->content = ft_strdup(tmp->f_name)))
			ssl_error(SSL_ERROR_MALLOC, meta, task_h);
		else if (!tmp->is_str && !ft_strequ(tmp->f_name, "stdin") && \
			(tmp->fd = open(tmp->f_name, O_RDONLY)) == -1)
			asprintf(&tmp->hash, "%s: %s", tmp->f_name, strerror(errno));
		if (!tmp->is_str && tmp->fd != -1)
			dgst_read_file(meta, task_h, tmp);
		tmp->msg_size *= 8;
		close(tmp->fd);
		if (tmp->is_str)
			 tmp->msg_size = ft_strlen(tmp->content) * 8;
		if (tmp->content)
			dgst_pad_content(meta, task_h, tmp);
	ft_memprnt(tmp->content, (tmp->msg_size + tmp->pad + 64) / 8);
	printf("  size: %ld", tmp->msg_size);
	printf("\n");
		tmp = tmp->next;
	}
}

/*
**	Simple function to display the content of the tasks. ======================
**	Will be deleted before turning-in the project. ============================
*/
void	dgst_print_content(t_task_h *task_h) {
	printf(" ===============> [%s] <===============\n", __FUNCTION__);
	for (t_task	*tmp = task_h->head; tmp; tmp = tmp->next) {
		printf("task:\tfile_name [%s]%*c", tmp->f_name, (int)(15 - ft_strlen(tmp->f_name)), ' ');
		// printf("fd [%i]\t", tmp->fd);
		printf("size [%.8ld]\t", tmp->msg_size);
		// printf("hash [%s]\t", tmp->hash);
		printf("content [%s] => %zu", tmp->content, tmp->msg_size + tmp->pad + 65);
		printf("\n");
	}
}

/*
**	The ssl_md5() function is the main function of the md5 command of the =====
**	ft_ssl programm. Upon entering the function, the optionnal flags are get ==
**	by the dgst_get_opts() function. Then, the different tasks are added to the
**	task list (see dgst_add_task() function for more infos). If the task list =
**	is empty after gathering, the standard input is read and concidered to be =
**	a task. Afterward, the list of takes is processed and the hash field of the
**	tasks is filled. ==========================================================
*/
int				ssl_md5(char **array)
{
	char			*read;
	static t_meta	meta;
	static t_task_h	task_h;

	if (dgst_get_opts(array, &meta, &task_h))
	{
		ssl_del_meta(&meta);			// del meta when an error occurs while reading opts.
		ssl_del_task(&task_h);			// del task when an error occurs while reading opts.
		return (1);
	}
	while (array[meta.nb_opts])
		dgst_add_task(&meta, &task_h, array[meta.nb_opts++], false);
	if (!task_h.len)
		dgst_add_task(&meta, &task_h, "stdin", false);
	dgst_get_content(&meta, &task_h);
	dgst_print_content(&task_h);

	// if (get_next_line(stdin->_fileno, &read) == -1)
	// 	ssl_error(SSL_ERROR_READ, &meta, &task_h);
	// if (!(ft_strequ(read, "exit") || ft_strequ(read, "quit") || !*read))
	// 	return (!printf("coming soon\n"));
	// ft_strdel(&read);
	ssl_del_meta(&meta);
	ssl_del_task(&task_h);
	return (0);
}