#include "ft_ssl.h"

/*
**	The dgst_add_task() function adds a new task to the list of task to do. A =
**	task is concidered to be the a file to hash. The file to hash is identified
**	by it's name. Into the function, the file isn't open, so it's file ========
**	descriptor isn't set. The "is_string" boolean specifies of the file is a ==
**	file or only a string to hash. ============================================
*/
void			dgst_add_task(t_meta *meta, t_task_h *task_h, char *f_name, t_bool is_str)
{
	t_task			*fresh;

	if (!(fresh = (t_task *)ft_memalloc(sizeof (*fresh))) || \
		!(fresh->f_name = ft_strdup(f_name)))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	fresh->is_str = is_str;
	fresh->header = task_h;
	task_h->len++;
	if (!task_h->head)
	{
		task_h->head = fresh;
		task_h->tail = fresh;
		return ;
	}
	fresh->prev = task_h->tail;
	task_h->tail->next = fresh;
	task_h->tail = fresh;
}

void			dgst_get_task(char **arr, t_meta *meta, t_task_h *task_h)
{
	while (arr[meta->nb_opts])
		dgst_add_task(meta, task_h, arr[meta->nb_opts++], 0);
}

/*
**	Simple function to display the tasks. =====================================
**	Will be deleted before turning-in the project. ============================
*/
void	dgst_print_task(t_task_h *task_h) {
	printf(" ===============> [%s] <===============\n", __FUNCTION__);
	for (t_task	*tmp = task_h->head; tmp; tmp = tmp->next) {
		printf("task:\tfile_name [%s]%*cis_string [%d]\n", \
			tmp->f_name, (int)(18 - ft_strlen(tmp->f_name)), ' ', \
			tmp->is_str);
	}
}