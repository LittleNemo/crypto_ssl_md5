#include "ft_ssl.h"

/*
**	The ssl_del_meta() function dumps the data stored in the meta structure. ==
**	This includes the potential name of the output file. After this deletion, =
**	the entier structure is set to zero. ======================================
*/
void			ssl_del_meta(t_meta *meta)
{
	if (!meta)
		return ;
	ft_strdel(&meta->out_name);
	if (meta->out_fd)
		close(meta->out_fd);
	bzero(meta, sizeof(meta));
}

/*
**	The ssl_del_task() function deletes the task linked list given. The tasks =
**	are created by the dgst_add_task() function and include the name of the ===
**	file to hash (file-name), the content of this file (content) and the hash =
**	of the file (hash). These character strings are deleted in every links of =
**	the list. Then, the allocated memory of the link is freed. ================
*/
void			ssl_del_task(t_task_h *task)
{
	t_task			*tmp;

	if (!task)
		return ;
	while (task->head)
	{
		tmp = task->head;
		task->head = task->head->next;
		ft_strdel(&tmp->f_name);
		ft_strdel(&tmp->content);
		if (tmp->fd != -1)				//	TO DELETE after hashing ahs been done
			ft_strdel(&tmp->hash);
		free(tmp);
	}
}