#include "ft_ssl.h"

int				ft_strcmp(const char *s1, const char *s2)
{
	while (*s1 || *s2)
		if (*s1++ != *s2++)
			return (*(unsigned char *)--s1 - *(unsigned char *)--s2);
	return (0);
}

int				ft_strequ(const char *s1, const char *s2)
{
	if (!s1 || !s2)
		return (0);
	return (!ft_strcmp(s1, s2));
}

int				ft_strncmp(const char *s1, const char *s2, size_t n)
{
	while (n-- && (*s1 || *s2))
		if (*s1++ != *s2++)
			return (*(unsigned char *)--s1 - *(unsigned char *)--s2);
	return (0);
}

int				ft_strnequ(const char *s1, const char *s2, size_t n)
{
	if (!s1 || !s2)
		return (0);
	return (!ft_strncmp(s1, s2, n));
}

int				ft_putstr_fd(char const *s, int fd)
{
	return (write(fd, s, ft_strlen(s)));
}

int				ft_putstr_endl_fd(const char *s, int fd)
{
	return (write(fd, s, ft_strlen(s)) + write(fd, "\n", 1));
}

int			ft_strdel(char **as)
{
	if (!as || !*as)
		return (0);
	free(*as);
	*as = NULL;
	return (1);
}

int			ft_tabdel(void **tab, size_t len)
{
	size_t			i;

	if (!tab)
		return (0);
	i = 0;
	while (i < len)
		free(tab[i++]);
	free(tab);
	return (1);
}

void			ft_puttab_fd(char **tab, int fd)
{
	int				i;

	if (!tab)
		return ;
	i = -1;
	while (tab[++i])
		ft_putstr_endl_fd(tab[i], fd);
}

size_t			ft_strlen(const char *s)
{
	size_t			len;

	len = 0;
	while (s[len])
		len++;
	return (len);
}

void			*ft_memcpy(void *dst, const void *src, size_t n)
{
	void			*d;

	d = dst;
	while (n--)
		*(unsigned char *)dst++ = *(unsigned char *)src++;
	return (d);
}

t_bool			ft_memdel(void **ap)
{
	if (!ap || !*ap)
		return (false);
	free(*ap);
	*ap = NULL;
	return (true);
}

char			*ft_strcpy(char *dst, const char *src)
{
	return ((char *)ft_memcpy(dst, src, ft_strlen(src) + 1));
}

char			*ft_strcat(char *dst, const char *src)
{
	ft_strcpy(dst + ft_strlen(dst), src);
	return (dst);
}

char			*ft_strchr(const char *s, int c)
{
	while (*s != c)
		if (!*s++)
			return (NULL);
	return ((char *)s);
}

size_t			ft_strcspn(const char *s, const char *charset)
{
	size_t			ret;

	ret = 0;
	while (*s && ++ret)
		if (ft_strchr(charset, *s++))
			return (ret - 1);
	return (ret);
}

size_t			ft_strspn(const char *s, const char *charset)
{
	size_t			ret;

	ret = 0;
	while (*s && ft_strchr(charset, *s++))
		ret++;
	return (ret);
}

void			*ft_memset(void *b, int c, size_t len)
{
	size_t			i;

	i = 0;
	while (i < len)
		((unsigned char *)b)[i++] = (unsigned char)c;
	return (b);
}

void			ft_bzero(void *s, size_t n)
{
	ft_memset(s, 0, n);
}

void			*ft_memalloc(size_t size)
{
	void			*fresh;

	if (!(fresh = (void *)malloc(sizeof(*fresh) * size)))
		return (NULL);
	ft_bzero(fresh, size);
	return (fresh);
}

size_t			ft_strnlen(const char *s, size_t maxlen)
{
	size_t			len;

	len = 0;
	while (len < maxlen)
		if (!s[len++])
			return (--len);
	return (maxlen);
}

char			*ft_strnew(size_t size)
{
	char			*fresh;

	if (!(fresh = (char *)malloc(sizeof(*fresh) * (++size))))
		return (NULL);
	ft_bzero(fresh, size);
	return (fresh);
}

char			*ft_strncpy(char *dst, const char *src, size_t len)
{
	size_t			n;

	n = ft_strnlen(src, len);
	if (n != len)
		ft_memset(dst + n, '\0', len - n);
	return ((char *)ft_memcpy(dst, src, n));
}

char			*ft_strsub(const char *s, unsigned int start, size_t len)
{
	char			*fresh;

	if (!s || !(fresh = ft_strnew(len)))
		return (NULL);
	return (ft_strncpy(fresh, s + start, len));
}

char			*ft_strjoin(const char *s1, const char *s2)
{
	char			*fresh;

	if (!s1 || !s2 || !(fresh = ft_strnew(ft_strlen(s1) + ft_strlen(s2))))
		return (NULL);
	ft_strcpy(fresh, s1);
	ft_strcat(fresh, s2);
	return (fresh);
}

char			*ft_strdup(const char *s)
{
	unsigned int	len;
	char			*fresh;

	len = ft_strlen(s);
	if (!(fresh = ft_strnew(len)))
		return (NULL);
	return ((char *)ft_memcpy(fresh, s, len));
}

char			*ft_strndup(const char *s, size_t n)
{
	unsigned int	len;
	char			*fresh;

	len = ft_strnlen(s, n);
	if (!(fresh = ft_strnew(len)))
		return (NULL);
	return ((char *)ft_memcpy(fresh, s, len));
}

static int		gnl_reader(const int fd, char **tab)
{
	char			*buff;
	char			*tmp;
	int				ret;

	if (!(buff = ft_strnew(BUFF_SIZE)))
		return (-1);
	ret = 1;
	while (!ft_strchr(tab[fd], '\n') && ((ret = read(fd, buff, BUFF_SIZE)) > 0))
	{
		buff[ret] = '\0';
		tmp = tab[fd];
		if (!(tab[fd] = ft_strjoin(tmp, buff)))
			return (-1);
		ft_strdel(&tmp);
	}
	ft_strdel(&buff);
	return (ret);
}

int				get_next_line(const int fd, char **line)
{
	static char		*tab[FOPEN_MAX];
	char			*tmp;
	int				ret;

	if ((fd < 0) || (fd > FOPEN_MAX) || (line == NULL))
		return (-1);
	if (tab[fd] == NULL && !(tab[fd] = ft_strnew(0)))
		return (-1);
	if ((ret = gnl_reader(fd, tab)) == -1)
		return (-1);
	if (ret == 0)
	{
		if (!(*line = ft_strdup(tab[fd])))
			return (-1);
		ft_strdel(&tab[fd]);
		return (!!ft_strlen(*line));
	}
	if (!(*line = ft_strsub(tab[fd], 0, ft_strchr(tab[fd], '\n') - tab[fd])))
		return (-1);
	tmp = tab[fd];
	if (!(tab[fd] = ft_strdup(ft_strchr(tmp, '\n') + 1)))
		return (-1);
	ft_strdel(&tmp);
	return (1);
}

int				ft_putfile_fd(const char *file_name, int fd)
{
	int				fd_file;
	int				len;
	char			*line;

	if ((fd_file = open(file_name, O_RDONLY)) == -1)
		return (-1);
	len = 0;
	while (get_next_line(fd_file, &line))
	{
		len += ft_putstr_endl_fd(line, fd);
		ft_strdel(&line);
	}
	ft_strdel(&line);
	if (close(fd_file) == -1)
		return (-1);
	return (len);
}

char			*ft_arrmrg(char **arr)
{
	char			*str;
	size_t			len;
	size_t			i;

	if (!arr)
		return (NULL);
	len = 0;
	i = 0;
	while (arr[i])
		len += strlen(arr[i++]) + 1;
	if (!(str = ft_strnew(len)))
		return (NULL);
	i = 0;
	while (arr[i])
	{
		strcat(str, arr[i++]);
		strcat(str, " ");
	}
	return (str);
}

static size_t	ft_cntwrd(const char *s, char *charset)
{
	size_t			nb_wrd;

	nb_wrd = 0;
	while (*s)
	{
		s += ft_strcspn(s, charset);
		s += ft_strspn(s, charset);
		if (*s)
			nb_wrd++;
	}
	return (nb_wrd);
}

char			**ft_strusplit(const char *s, char *charset)
{
	char			**tab;
	size_t			len;
	size_t			i;

	if (!s || !charset || !(tab = (char **)ft_memalloc(sizeof(tab) \
		* (ft_cntwrd(s, charset) + 1))))
		return (NULL);
	i = 0;
	s += ft_strspn(s, charset);
	while (*s)
	{
		len = ft_strcspn(s, charset);
		if (!(tab[i++] = ft_strndup(s, len)))
			return (NULL);
		s += len + ft_strspn(s, charset);
	}
	return (tab);
}

char			**ft_strsplit(const char *s, char c)
{
	return (ft_strusplit(s, &c));
}

int			ft_tablen(char **tab)
{
	int			size;

	size = 0;
	while (tab[size])
		size++;
	return (size);
}
