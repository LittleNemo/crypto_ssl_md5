#include "ft_ssl.h"

/*
**	The ssl_cntline() function counts the number of lines into the string s. ==
**	A line is concidered to be a string of characters separeted by ============
**	whitespaces or within quotes ('"'). =======================================
**	This function is called by the ssl_normalize_input() function. ============
*/
static size_t	ssl_cntline(const char *s)
{
	size_t			nb_line;

	nb_line = 0;
	while (*s)
	{
		if (*s == '"')
			s += ft_strcspn(s + 1, "\"") + 1;
		s += ft_strcspn(s, " \t\n\v\f\r");
		s += ft_strspn(s, " \t\n\v\f\r");
		nb_line++;
	}
	return (nb_line);
}
/*
**	The ssl_normalize_input() function is similar to the ft_strsplit() function
**	of the libft. It takes a string and split it on the whitespaces into an ===
**	array, but does not split strings into quotes ('"'). ======================
*/
char			**ssl_normalize_input(const char *s)
{
	char			**tab;
	size_t			len;
	size_t			i;

	if (!s || !(tab = (char **)ft_memalloc(sizeof(tab) * (ssl_cntline(s) + 1))))
		return (NULL);
	i = 0;
	s += ft_strspn(s, " \t\n\v\f\r");
	while (*s)
	{
		len = ft_strcspn(s, " \t\n\v\f\r") + 1;
		if (*s == '"')
			len = ft_strcspn(s + 1, "\"") + 1;
		if (!(tab[i++] = ft_strndup(s + (*s == '"'), len - 1)))
			return (NULL);
		s += len + (*s == '"') * 2 + ft_strspn(s, " \t\n\v\f\r");
	}
	return (tab);
}

static size_t	ft_countchar(const char *s, char c)
{
	size_t			count;

	count = 0;
	while (*s)
		if (*s++ == c)
			count++;
	return (count);
}

/*
**	The ft_ssl program can be launch with or without an argument. If an =======
**	argument is given, the program will check it and execute the associated ===
**	function (md5, sha256, ...). If no argument are given, the program will ===
**	enter its main loop. ======================================================
**	Upon entering the main loop, a prompt is printed on the standard output. ==
**	Then, the standard input is read and the line is given to the hash function
**	selector. =================================================================
**	To exit this loop, the user can type "exit" or "quit" in the input. =======
*/
int				main(int ac, char **av)
{
	char			*line;
	char			**array;
	t_bool			keep_looping;

	if (ac >= 2)
		return (ssl_select_hash(av + 1));
	keep_looping = true;
	while (keep_looping)
	{
		ft_putstr_fd(SSL_PROMPT, 1); // ft_printf("%@%s %@", GREEN, SSL_PROMPT, EOC);
		if (get_next_line(stdin->_fileno, &line) == -1)
			ssl_error(SSL_ERROR_READ, NULL, NULL);
		if ((ft_countchar(line, '"') % 2) && ft_strdel(&line) && fprintf(stderr, "%s\n", SSL_MSG_QUOTE))
			continue ;
		if (!(array = ssl_normalize_input(line)))
			ssl_error(SSL_ERROR_MALLOC, NULL, NULL);
		if (ft_strequ(line, "exit") || ft_strequ(line, "quit") || !*line)
			keep_looping = false;
		else
			ssl_select_hash(array);
		ft_strdel(&line);
		ft_tabdel((void **)array, ft_tablen(array));
	}
	return (0);
}