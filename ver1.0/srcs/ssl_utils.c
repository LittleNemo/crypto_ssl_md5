#include "ft_ssl.h"

void			dgst_get_padding(t_task *tmp)
{
	while ((tmp->msg_size + 1 + 64 + tmp->pad) % 512)
		tmp->pad++;
}