#include "ft_ssl.h"

/*
**	The ssl_get_errors() function is a singleton that gives an array of the ===
**	global options functions. This function is called by the ssl_error ========
**	function. =================================================================
*/
static int		(**dgst_get_opts_func(void))(char **, t_meta *, t_task_h *)
{
	static int		(*tab[10])(char **, t_meta *, t_task_h *);
	
	if (!tab[0])
	{
		tab[0] = dgst_opts_out;
		tab[1] = dgst_opts_out;
		tab[2] = dgst_opts_print;
		tab[3] = dgst_opts_print;
		tab[4] = dgst_opts_quiet;
		tab[5] = dgst_opts_quiet;
		tab[6] = dgst_opts_reverse;
		tab[7] = dgst_opts_reverse;
		tab[8] = dgst_opts_string;
		tab[9] = dgst_opts_string;
	}
	return (tab);
}

/*
**	The dgst_get_opts() function collects the options from command lines for ==
**	the differents Message Digest functions (a list of these functions can be =
**	found in the ./rscs/help.txt file). If a string does not match with any ===
**	options, a helping message is print and the function returns 1. ===========
*/
int				dgst_get_opts(char **arr, t_meta *meta, t_task_h *task_h)
{
	size_t			id;
	int				(**opts_funcs)(char **, t_meta *, t_task_h *);
	char			*opts_name[11] = {
		"--out", "-o", "--print", "-p",
		"--quiet", "-q", "--reverse", "-r", "--string", "-s",
		NULL 
	};

	opts_funcs = dgst_get_opts_func();
	while (arr[++meta->nb_opts] && arr[meta->nb_opts][0] == '-' && !ft_strequ("--", arr[meta->nb_opts]))
	{
		id = 0;
		while (opts_name[id] && !ft_strequ(opts_name[id], arr[meta->nb_opts]))
			id++;
		if (!opts_name[id])
			return (!!fprintf(stderr, "Invalid option. %s\n", SSL_MSG_HELP));
		if (opts_funcs[id](arr, meta, task_h))
			return (1);
	}
	if (ft_strequ("--", arr[meta->nb_opts]))
		meta->nb_opts++;
	return (0);
} 