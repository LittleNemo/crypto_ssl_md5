#include "ft_ssl.h"

/*
**	The dgst_opts_out() function gets the name of the file where the output ===
**	will be written. The name of the file should be the next entry of the =====
**	array. If no entry are present, an error message is printed and the =======
**	function returns. If an entry is present, the name is copied to the =======
**	"out_name" field of the meta structure, the "out" bit of the "opts" field =
**	is set to 1 and the "nb_opts" field is increased. Every time this =========
**	function is called, the name will be updated to be the last one. ==========
**	This function is called whenever the flags "-o" or "--out" are present into
**	the command line. =========================================================
*/
int				dgst_opts_out(char **arr, t_meta *meta, t_task_h *task_h)
{
	if (!arr[meta->nb_opts + 1])
		return (!!fprintf(stderr, "Option %s needs a value.\n", arr[meta->nb_opts]));
	ft_strdel(&meta->out_name);
	if (!(meta->out_name = ft_strdup(arr[meta->nb_opts + 1])))
		ssl_error(SSL_ERROR_MALLOC, meta, task_h);
	meta->opts |= (1 << DGST_OTPS_OUT);
	meta->nb_opts++;
	return (0);
}

/*
**	The dgst_opts_print() function adds a mew task to the task list. This =====
**	task will be reading the standard input. This will happen only once per ===
**	command, regardless of the number of call to this function. ===============
**	This function is called whenever the flags "-p" or "--print" are present ==
**	into the command line. ====================================================
*/
int				dgst_opts_print(char **arr, t_meta *meta, t_task_h *task_h)
{
	if (!(meta->opts & (1 << DGST_OPTS_PRINT)))
		dgst_add_task(meta, task_h, "stdin", false);
	meta->opts |= (1 << DGST_OPTS_PRINT);
	return (0);
}

/*
**	The dgst_opts_quiet() function sets the "quiet" bit of the opts field in ==
**	the meta structure to 1. ==================================================
**	This function is called whenever the flags "-r" or "--reverse" are ========
**	present into the command line. ============================================
*/
int				dgst_opts_quiet(char **arr, t_meta *meta, t_task_h *task_h)
{
	(void)task_h;
	meta->opts |= (1 << DGST_OPTS_QUIET);
	return (0);
}

/*
**	The dgst_opts_reverse() function sets the "reverse" bit of the opts field =
**	in the meta structure to 1. ===============================================
**	This function is called whenever the flags "-r" or "--reverse" are ========
**	present into the command line. ============================================
*/
int				dgst_opts_reverse(char **arr, t_meta *meta, t_task_h *task_h)
{
	(void)task_h;
	meta->opts |= (1 << DGST_OPTS_REVERSE);
	return (0);
}

/*
**	The dgst_opts_string() function adds a new task to the task list. This task
**	should be the next entry of the array. If no entry are present, an error ==
**	message is printed and the function returns. If an entry is present, the ==
**	task is added and the "nb_opts" field of the meta structure is increased. =
**	This function is called whenever the flags "-s" or "--string" are present =
**	into the command line. ====================================================
*/
int				dgst_opts_string(char **arr, t_meta *meta, t_task_h *task_h)
{
	if (!arr[meta->nb_opts + 1])
		return (!!fprintf(stderr, "Option %s needs a value.\n", arr[meta->nb_opts]));
	dgst_add_task(meta, task_h, arr[meta->nb_opts + 1], true);
	meta->nb_opts++;
	return (0);
}