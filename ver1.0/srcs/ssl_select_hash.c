#include "ft_ssl.h"

/*
**	The ssl_get_funcs() function is a singleton that gives an array filled ====
**	with pointers on the differents hash functions (md5, sha256, ...). ========
**	This function is called by the ssl_select_hash function. ==================
*/
static int		(**ssl_get_funcs(void))(char **)
{
	static int		(*tab[4])(char **);
	
	if (!tab[0])
	{
		tab[0] = ssl_print_help;
		tab[1] = ssl_sha256;
		tab[2] = ssl_md5;
		tab[3] = ssl_sha256;
	}
	return (tab);
}

/*
**	The ssl_select_hash() function is a function dispatcher from the main =====
**	function to the differents hash functions (md5, sha256, ...). =============
**	The function takes the a character string and search the hash function ====
**	corresponding to the first word of the string. If no function is ==========
**	found, the functions prints an error message and returns. =================
*/
int				ssl_select_hash(char **array)
{
	size_t			id;
	int				(**hash_funcs)(char **);
	char			*hash_names[5] = {
		"help",
		"dgst", "md5", "sha256",
		NULL
	};

	id = 0;
	hash_funcs = ssl_get_funcs();
	while (hash_names[id] && !ft_strequ(array[0], hash_names[id]))
		id++;
	if (!hash_names[id])
		return (!!fprintf(stderr, "Invalid command. %s\n", SSL_MSG_HELP));
	return (hash_funcs[id](array));
}