#ifndef FT_SSL_H
# define FT_SSL_H

// # include "libft_part_io.h"
// # include "libft_part_memory.h"
// # include "libft_part_string.h"
// # include "libft_part_type.h"
// # include "ft_printf.h"
#include <sys/types.h> 			//	TO DELETE
# include "ft_ssl_defines.h"
# include "ft_ssl_typedefs.h"
# include <errno.h>

void			ssl_error(int code, t_meta *meta, t_task_h *task_h);
int				ssl_select_hash(char **array);

void			dgst_get_task(char **arr, t_meta *meta, t_task_h *task_h);		// MAY NOT BE USED
void			dgst_add_task(t_meta *meta, t_task_h *task_h, char *f_name, t_bool is_str);
void			dgst_print_task(t_task_h *task_h);								// TO DELETE

int				dgst_get_opts(char **arr, t_meta *meta, t_task_h *task_h);
int				dgst_opts_out(char **arr, t_meta *meta, t_task_h *task_h);
int				dgst_opts_print(char **arr, t_meta *meta, t_task_h *task_h);
int				dgst_opts_quiet(char **arr, t_meta *meta, t_task_h *task_h);
int				dgst_opts_reverse(char **arr, t_meta *meta, t_task_h *task_h);
int				dgst_opts_string(char **arr, t_meta *meta, t_task_h *task_h);

int				ssl_print_help(char **array);
int				ssl_md5(char **array);
int				ssl_sha256(char **array);

void			dgst_get_padding(t_task *tmp);

void			ssl_del_meta(t_meta *meta);
void			ssl_del_task(t_task_h *task_h);


// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFF_SIZE 1024

char			*ft_arrmrg(char **arr);

size_t			ft_strspn(const char *s, const char *charset);
size_t			ft_strcspn(const char *s, const char *charset);
char			*ft_strnew(size_t size);
int				ft_strequ(const char *s1, const char *s2);
int				ft_strnequ(const char *s1, const char *s2, size_t n);
char			*ft_strjoin(const char *s1, const char *s2);
size_t			ft_strlen(const char *s);
int				ft_strdel(char **as);
int				ft_tabdel(void **tab, size_t len);
void			*ft_memalloc(size_t size);
void			ft_bzero(void *s, size_t n);
void			*ft_memset(void *b, int c, size_t len);
void			*ft_memcpy(void *dst, const void *src, size_t n);
char			*ft_strdup(const char *s);
t_bool			ft_memdel(void **ap);
char			*ft_strndup(const char *s, size_t n);
int				ft_putstr_fd(char const *s, int fd);
int				ft_putstr_endl_fd(const char *s, int fd);
void			ft_puttab_fd(char **tab, int fd);
int				get_next_line(const int fd, char **line);
int				ft_putfile_fd(const char *file_name, int fd);
char			**ft_strusplit(const char *s, char *charset);
char			**ft_strsplit(const char *s, char c);
int			ft_tablen(char **tab);
// ============================================================================

#endif