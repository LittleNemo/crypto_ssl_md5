#ifndef FT_SSL_DEFINES_H
# define FT_SSL_DEFINES_H

# define SSL_RSC_HELP	"./rscs/help.txt"
# define SSL_PROMPT		"ft_ssl>"

# define SSL_MSG_HELP	"Use \"help\" to see more infos."
# define SSL_MSG_QUOTE	"Please, close quotes and retry."
# define SSL_MSG_MALLOC	"malloc failure"
# define SSL_MSG_READ	"read failure"

# define DGST_ONE_BIT	0b10000000
# define DGST_NUL_BIT	0b00101110

#endif